import { DestinoViaje } from './destino-viaje';
import { reducerDestinosViajes, DestinosViajesStates, intializeDestinosViajesState,
            InitMyDataAction, NuevoDestinoAction } from './destinos-viajes-states.model';

describe('Destino Viaje', () => {
    it('should reduce init data', () => {
        // setup
        const prevState: DestinosViajesStates = intializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        // action
        const newState: DestinosViajesStates = reducerDestinosViajes(prevState, action);
        // assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it('should reduce new item added', () => {
        // setup
        const prevState: DestinosViajesStates = intializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
        // action
        const newState: DestinosViajesStates = reducerDestinosViajes(prevState, action);
        // assertions
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });
});
