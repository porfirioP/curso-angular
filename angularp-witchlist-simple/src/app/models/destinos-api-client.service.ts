import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
// import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfig, AppState, APP_CONFIG, db } from '../app.module';
import { DestinoViaje } from './destino-viaje';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-states.model';

@Injectable()
export class DestinosApiClientService {
  destinos: DestinoViaje[] = [];

  constructor(private store: Store<AppState>,
              @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
              private http: HttpClient) {
    this.store.select(state => state.destinos).subscribe((data) => {
      console.log('destinos sub store');
      console.log(data);
      this. destinos = data.items;
    });
    this.store.subscribe((data) => {
          console.log('all store');
          console.log(data);
    });
  }

  add = (destinoViaje: DestinoViaje) => {
    const header: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN':  'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: destinoViaje.nombre}, { headers: header});
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if (data.status === 200) {
        this.store.dispatch(new NuevoDestinoAction(destinoViaje));
        const myDb = db;
        myDb.destinos.add(destinoViaje);
        console.log('todos los destinos de la db!');
        myDb.destinos.toArray().then(destinos => console.log(destinos));
      }
    });
  }

  getById(id: string): DestinoViaje {
    console.log(id);
    return this.destinos.filter((d) => d.id.toString() === id)[0];
  }

  elegir = (destino: DestinoViaje) => {
    this.store.dispatch(new ElegidoFavoritoAction(destino));
  }

}
