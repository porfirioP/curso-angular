export class DestinoViaje {
  public id: number;
  private selected: boolean;
  public servicios = ['tina', 'desayuno'];

  constructor(
    public nombre: string,
    public imagenUrl: string,
    public votes: number = 0
  ) {}

  isSelected = () => {
    return this.selected;
  }

  setSelected = (select: boolean) => {
    this.selected = select;
  }

  voteUp = () => {
    this.votes++;
  }

  voteDown = () => {
    this.votes--;
  }
}
