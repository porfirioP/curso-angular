import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { from, fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { DestinoViaje } from '../../models/destino-viaje';
import { APP_CONFIG, AppConfig } from '../../app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.scss']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() itemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 5;
  searchResult: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.itemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidador,
        this.nombreValidatorParametrizable(this.minLongitud)])],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el form: ' + form);
    });
  }

  ngOnInit(): void {
    const eleNombre = document.getElementById('nombre') as HTMLInputElement;
    fromEvent(eleNombre, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(ajaxResponse => this.searchResult = ajaxResponse.response);
  }

  guardar = (nombre: string, url: string) => {
    const d = new DestinoViaje(nombre, url);
    this.itemAdded.emit(d);
    return false;
  }

  nombreValidador = (control: FormControl): { [s: string]: boolean } => {
    const l = control.value.toString().trim().length;
    return l > 0 && l < 5 ? { invalidNombre: true } : null;
  }

  nombreValidatorParametrizable = (minLong: number): ValidatorFn => {
    return (control: FormControl): {[s: string]: boolean} | null => {
      const l = control.value.toString().trim().length;
      return l > 0 && l < minLong ? { minLongNombre: true } : null;
    };
  }
}
