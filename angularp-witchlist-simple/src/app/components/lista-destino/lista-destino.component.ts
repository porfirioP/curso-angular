import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje';
import { DestinosApiClientService } from '../../models/destinos-api-client.service';

@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.scss'],
  providers: [DestinosApiClientService]
})
export class ListaDestinoComponent implements OnInit {
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient: DestinosApiClientService, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito).subscribe(data => {
      if (data !== null) {
        this.updates.push('se ha elegido a: ' + data.nombre);
      }
    });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }

  agregado = (destinoViaje: DestinoViaje): void => {
    this.destinosApiClient.add(destinoViaje);
    this.onItemAdded.emit(destinoViaje);
  }

  elegido = (destino: DestinoViaje) => {
    this.destinosApiClient.elegir(destino);
  }

}
