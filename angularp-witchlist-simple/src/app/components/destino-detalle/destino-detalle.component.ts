import { Component, Inject, Injectable, InjectionToken, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { DestinoViaje } from 'src/app/models/destino-viaje';
import { DestinosApiClientService } from 'src/app/models/destinos-api-client.service';

// @Injectable()
// export class DestinosApiClientViejo {
//   getById(id: string): DestinoViaje {
//     console.log('llamado por la clave vieja!' + id);
//     return null;
//   }
// }

// interface AppConfig {
//   apiEndpoint: string;
// }

// const APP_CONFIG_VALUE: AppConfig = {
//   apiEndpoint: 'mi_api.com'
// };

// const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

// @Injectable()
// export class DestinosApiClientDecorate extends DestinosApiClientService {
//   constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>) {
//     super(store);
//   }
//   getById(id: string): DestinoViaje {
//     console.log('llamado por la clase decorada');
//     console.log('config: ' + this.config.apiEndpoint);
//     return super.getById(id);
//   }
// }

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.scss'],
  providers: [DestinosApiClientService,
  // { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
  // { provide: DestinosApiClientService, useClass: DestinosApiClientDecorate },
  // { provide: DestinosApiClientViejo, useExisting: DestinosApiClientService },
]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;
  style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json',
      },
    },
    version: 8,
    layers: [
      {
        id: 'countries',
        type: 'fill',
        source: 'world',
        layout: {},
        paint: {
          'fill-color': '#6F788A'
        },
      },
    ],
  };
  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClientService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
