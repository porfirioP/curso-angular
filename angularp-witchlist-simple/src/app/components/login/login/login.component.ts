import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  mensajeError: string;

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
    this.mensajeError = '';
  }
  login = (username: string, password: string) => {
    this.mensajeError = '';
    if (!this.authService.login(username, password)) {
      this.mensajeError = 'Login incorrecto.';
      // tslint:disable-next-line: typedef
      setTimeout(function() {
        this.mensajeError = '';
      }.bind(this), 2500);
      return false;
    }
  }

  logout = (): boolean => {
    this.authService.logout();
    return false;
  }
}
