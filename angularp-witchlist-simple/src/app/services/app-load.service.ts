import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG_VALUE } from '../app.module';
import { InitMyDataAction } from '../models/destinos-viajes-states.model';

export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}

@Injectable({
  providedIn: 'root'
})
export class AppLoadService {

  constructor(private store: Store<AppState>, private http: HttpClient) { }

  async initializeDestinosViajesState(): Promise<any> {
    const header: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN':  'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: header});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
